Données utilisées pour le projet PROMOSOL DEMELER

* table_metadata.csv : index des données.
* table_met_promosol.csv : données des réseaux expérimentaux 2000, 2001, 2020, 2021.
* table_modeling_promosol.csv : données calculées (écarts de rendement, données climatiques) utilisées pour la modélisation.